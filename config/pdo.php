<?php if(!defined('SIDELOADED')) { die('Direct access is not allowed'); }

return [
	'main' => [
		'driver'   => 'mysql',
		'host'     => 'localhost',
		'port'     => '3306',
		'database' => '',
		'username' => 'root',
		'password' => ''
	]
];
