<?php if(!defined('SIDELOADED')) { die('Direct access is not allowed'); }

// These should be correct by themselves, only change them if required
define('PATH', str_replace('/index.php', '', $_SERVER['SCRIPT_FILENAME']));
define('URL', 'http://' .  $_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']));

// You can change these in order to configure Freemwurk
define('FW_SESSIONMANAGER', true);
define('FW_COOKIEMANAGER', false);
define('FW_DBDRIVER', 'Freemwurk\Core\Db\Mysql');

// You should include any custom defines below this line
define('THEME', 'default');
define('LAYOUT', 'default');

