<?php if(!defined('SIDELOADED')) { die('Direct access is not allowed.'); }

return [
	'file' => 'home',
	'cont' => 'Home',
	'func' => 'index',

	'error_controller' => 'error',
	'error_method'     => 'notfound'
];
