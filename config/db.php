<?php if(!defined('SIDELOADED')) { die('Direct access is not allowed'); }

return [
	'host'     => 'localhost',
	'username' => 'root',
	'passwd'   => '',
	'dbname'   => 'test'
];
