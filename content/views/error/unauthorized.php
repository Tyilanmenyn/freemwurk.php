<section>
	<h1>Unauthorized!</h1>
	<p>
		This page is only available to certain people. You must log in to prove you
		have the permissions to view this page.
	</p>
</section>
