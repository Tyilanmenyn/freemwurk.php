<section>
	<h1>An unknown error has occurred!</h1>
	<p>
		Something went wrong on our side. If this is happening more often, please
		contact the site administrator.
	</p>
</section>
