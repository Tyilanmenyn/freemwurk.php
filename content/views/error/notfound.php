<section>
	<h1>File not found!</h1>
	<p>
		The file you requested at <strong><?=current_uri(true)?></strong> could not be found on
		the server.
	</p>
	<p>
		If you typed the URL manually, you might want to check it for typos. If you
		were redirected here, you can try to contact the website's administrator to
		fix this issue.
	</p>
</section>
