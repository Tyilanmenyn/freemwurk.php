<section>
	<h1>Access forbidden!</h1>
	<p>
		You don't have the permission to access this page.
	</p>
	<p>
		You may have to authenticate in order to access this page.
	</p>
	<p>
		If you are already authenticated, but you still get this message, you simply
		are not allowed to access this page of the website.
	</p>
	<p>
		If you are the site's administrator and keep getting this message, you may
		have to modify the file permissions.
	</p>
</section>
