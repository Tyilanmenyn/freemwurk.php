<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Debug report</title>
		<style>
			pre {
				margin: 0;
				padding: 0;
			}
			.hidden {
				display: none;
			}
			.header {
				font-size: 1.5em;
				font-weight: bold;
			}
			.toggle a {
				color: inherit;
				text-decoration: none;
			}
			.toggle:before { content: "[ "; }
			.toggle:after  { content: " ]"; }
		</style>
		<script>
			function toggleVisibility(block) {
				console.log();
				if(document.getElementById(block).className == "hidden") {
					document.getElementById(block).className = "";
					document.getElementById(block).parentNode.getElementsByTagName('a')[0].innerHTML = "▽";
				} else {
					document.getElementById(block).className = "hidden";
					document.getElementById(block).parentNode.getElementsByTagName('a')[0].innerHTML = "▷";
				}
			}
		</script>
	</head>
	<body>
		<div>
			<h1>An exception has been caught</h1>
<?php if($exception->getCode() > 0): ?>
			<p>Error code <strong><?=$exception->getCode()?></strong></p>
<?php endif; ?>
		</div>
		<div>
			<p>
				<span class="header">Error Message</span>
				<span class="toggle"><a href="javascript:toggleVisibility('message')">▽</a></span>
			</p>
			<pre id="message"><?=$exception->getMessage()?></pre>
		</div>
		<div>
			<p>
				<span class="header">File</span>
				<span class="toggle"><a href="javascript:toggleVisibility('file')">▷</a></span>
			</p>
			<pre id="file" class="hidden"><?=highlight_string(file_get_contents($exception->getFile()))?></pre>
		<div>
			<p>
				<span class="header">Stack trace</span>
				<span class="toggle"><a href="javascript:toggleVisibility('stacktrace')">▷</a></span>
			</p>
			<pre class="hidden" id="stacktrace"><?=print_r($exception->getTrace(), true)?></pre>
		</div>
		<div>
			<p>
				<span class="header">Current $_SESSION</span>
				<span class="toggle"><a href="javascript:toggleVisibility('session')">▷</a></span>
			</p>
			<pre class="hidden" id="session"><?=print_r($_SESSION, true)?></pre>
		</div>
		<div>
			<p>
				<span class="header">Current $_COOKIE</span>
				<span class="toggle"><a href="javascript:toggleVisibility('cookie')">▷</a></span>
			</p>
			<pre class="hidden" id="cookie"><?=print_r($_COOKIE, true)?></pre>
		</div>
		<div>
			<p>
				<span class="header">Current $_SERVER</span>
				<span class="toggle"><a href="javascript:toggleVisibility('server')">▷</a></span>
			</p>
			<pre class="hidden" id="server"><?=print_r($_SERVER, true)?></pre>
		</div>
	</body>
</html>
