<?php

namespace Freemwurk\Controllers;

class ErrorController extends \Freemwurk\Core\Controller {

	public function unknown() { // {{{

		$this->v('layout/' . LAYOUT, [
			'title'   => 'Unknown error!',
			'content' => $this->v('error/unknown')
		], true);

	} // }}}

	public function forbidden() { // {{{

		header('HTTP/1.0 403 Forbidden');

		$this->v('layout/' . LAYOUT, [
			'title' => 'Forbidden!',
			'content' => $this->v('error/forbidden')
		], true);

	} // }}}

	public function notfound() { // {{{

		header('HTTP/1.0 404 Not Found');

		$this->f('current_uri');

		$this->v('layout/' . LAYOUT, [
			'title' => 'Not found!',
			'content' => $this->v('error/notfound')
		], true);

	} // }}}

	public function unauthorized() { // {{{

		header('HTTP/1.0 401 Unauthorized');

		$this->v('layout/' . LAYOUT, [
			'title' => 'Unauthorized!',
			'content' => $this->v('error/unauthorized')
		], true);

	} // }}}

}
