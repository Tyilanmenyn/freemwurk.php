<?php

// Allow Freemwurk to load it's requirements
define('SIDELOADED', true);

// Get the special defines configuration
require 'config/defines.php';

// Call out to our required files
require PATH . '/core/dataclass.php';

if(FW_SESSIONMANAGER) {
	// Include the sessionmanager if required

	require PATH . '/core/session.php';
	\Freemwurk\Core\Session::init();

}

if(FW_COOKIEMANAGER) {
	// Include the cookiemanager is required

	require PATH . '/core/cookie.php';

}

// Call all files to process the pagecall
require PATH . '/core/loader.php';
require PATH . '/core/controller.php';
require PATH . '/core/model.php';
require PATH . '/core/router.php';

// Let the router do it's work

if(FW_SESSIONMANAGER) {

	// Clear the tmp namespace
	\Freemwurk\Core\Session::clear('tmp');

}

