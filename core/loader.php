<?php

namespace Freemwurk\Core;

class Loader {

	protected $path,
	          $uri,
	          $controller,
	          $object,
	          $model;

	public function __construct() { // {{{

		$this->path = PATH;
		$this->uri  = URL;

		// Create empty dataclasses to store whatever is being loaded
		$this->controller = new dataClass();
		$this->model      = new dataClass();
		$this->object     = new dataClass();

	} // }}}

	/**
	 * @access protected
	 * @param $func The name of the function you wish to load.
	 * @return void
	 * Include a function. Functions will be searched for in /core/functions and
	 * /custom/functions. If the specified function is not found, an error will
	 * be thrown.
	 */
	protected function f($func) { // {{{

		$file[] = $this->path . '/custom/functions/' . $func . '.php';
		$file[] = $this->path . '/core/functions/' . $func . '.php';
		$file[] = $this->path . '/custom/functions/' . $func . '/';
		$file[] = $this->path . '/core/functions/' . $func . '/';

		$namespace = '\Freemwurk\Functions\\';
		$function  = $namespace . str_replace('/', '\\', $func);

		if(!function_exists($function)) {

			foreach($file as $f) {

				if(file_exists($f)) {

					require_once $f;
					break;

				}

			}

		}

		$args = func_get_args();
		array_shift($args);

		return call_user_func_array($function, $args);

	} // }}}

	/**
	 * @access protected
	 * @param string $name The name of the controller you wish to load.
	 * @return mixed Returns the object requested
	 */
	protected function c($name) { // {{{

		$controller = '\Freemwurk\Controllers\\' . ucfirst($name) . 'Controller';

		if(!class_exists($controller)) {

			$file = $this->path . '/content/controllers/' . $name . '.php';

			if(file_exists($file)) {

				require_once $file;

				$this->controller->$name = new $controller();

			} else {

				throw new \Exception('The controller "' . $name . '" could not be found');

			}

			return $this->controller->$name;

		}

	} // }}}

	/**
	 * @access protected
	 * @param $model The name of the model you wish to load.
	 * @return void
	 * Include a model to the current controller. The model can be accessed using
	 * $this->model->$model, or $this->model->$name if the $name parameter is
	 * set. Additionally, it is possible to pass along an array of extra
	 * variables using the $vars parameter.
	 */
	protected function m($name) { // {{{

		$model = '\Freemwurk\Models\\' . ucfirst($name) . 'Model';

		if(!class_exists($model)) {

			$file  = $this->path . '/content/models/' . $name . '.php';

			if(file_exists($file)) {

				require_once $file;

				$this->model->$name = new $model();

			} else {

				throw new \Exception('The model "' . $name . '" could not be found');

			}

		}

		return $this->model->$name;

	} // }}}

	/**
	 * @access protected
	 * @param $name The name of the object you wish to load.
	 * @return void
	 * Include an object to the current controller. The object can be accessed
	 * using $this->object->$object, or $this->object->$name if the $name
	 * parameter is set. Additionally, it is possible to pass along an array of
	 * extra variables using the $vars parameter.
	 */
	protected function o($name) { // {{{

		$object = '\Freemwurk\Objects\\' . ucfirst($name) . 'Object';

		if(!class_exists($object)) {

			$file[0] = $this->path . '/core/objects/' . $name . '.php';
			$file[1] = $this->path . '/custom/objects/' . $name . '.php';

			if(file_exists($file[0])) {
				// Check if the object exists as part of the core

				require_once $file[0];

			} elseif(file_exists($file[1])) {
				// Check if the object exists as part of the custom component

				require_once $file[1];

			} else {
				// Not found, throw error

				throw new \Exception('The object "' . $name . '" could not be found');
				return;

			}

			$this->object->$name = new $object();

		}

		return $this->object->$name;

	} // }}}

	/**
	 * @access protected
	 * @param $view The view you wish to load.
	 * @param $vars [Optional] An array of variables which should be accessible
	 *        within the view.
	 * @param $echo [Optional] Wether to return or echo the output. Defaults to
	 *        returning the echo, unless set to true.
	 * @return mixed Will either return the view if $echo is true, or it will
	 *         void.
	 * Load a view by the name $view. If necessary, an array of $var => $val
	 * pairs can be given as the $vars parameter. The variables will be
	 * accessible within the view by their $var names. By default, the output of
	 * the view is returned, unless $echo is set to true.
	 */
	protected function v($view, $vars = [], $echo = false) { // {{{

		$file = $this->path . '/content/views/' . $view . '.php';

		if(file_exists($file)) {

			ob_start();

			// Parse along the variables
			if(isset($vars) && count($vars) > 0) {

				foreach($vars as $key => $var) {

					$$key = $var;

				}

			}

			require $file;

			if($echo) {

				echo ob_get_clean();
				return;

			} else {

				return ob_get_clean();

			}

		} else {
			// 404

			throw new \Exception('The view "' . $view . '" could not be found');

		}

	} // }}}

	/**
	 * Load an SQL file for use in a query
	 *
	 * @param string $sql
	 * @access public
	 * @return void
	 */
	public function sql($sql) { // {{{

		$file = PATH . '/content/sql/' . $sql . '.sql';

		if(file_exists($file)) {

			return file_get_contents($file);

		} else {

			throw new \Exception('The SQL file "' . $sql . '" could not be found');

		}

	} // }}}

}

