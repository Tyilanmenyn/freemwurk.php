<?php
/**
 * @package Freemwurk
 * @since v1.0
 * @author Patrick "Tyil" Spek <p.spek@tyil.nl>
 * This is the main model class. All other models are extensions of this class,
 * and therefore inherit all functionality.
 */

namespace Freemwurk\Core;

class Model extends Loader {

	/**
	 * The construct should always be called in order to make sure that objects
	 * can be loaded properly.
	 */
	public function __construct() { // {{{

		parent::__construct();

	} // }}}

}
