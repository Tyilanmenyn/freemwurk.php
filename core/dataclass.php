<?php

namespace Freemwurk\Core;

class dataClass {

	private $data;

	public function __construct($arr = null) { // {{{

		$this->data = new \stdClass();

		if(is_array($arr)) {

			foreach($arr as $key => $val) {

				$this->$key = $val;

			}

		}

	} // }}}

	public function __set($name, $value) { // {{{

		@$this->data->$name = $value;

	} // }}}

	public function __get($name) { // {{{

		return @$this->data->$name;

	} // }}}

	/**
	 * Clear the object of any data.
	 */
	public function clear() { // {{{

		$this->data = new stdClass();

	} // }}}

	/**
	 * Return all data contained in this object.
	 * @return string
	 */
	public function dump() { // {{{

		return print_r($this->data, true);

	} // }}}

	public function toArray() { // {{{

		$arr = [];

		foreach($this->data as $key => $val) {

			$arr[$key] = $val;

		}

		return $arr;

	} // }}}

}
