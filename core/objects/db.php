<?php
/**
 * @author Patrick "Tyil" Spek <p.spek@tyil.nl>
 */

namespace Freemwurk\Objects;

class DbObject {

	protected $connection;
	protected $statements;

	public function __construct($mysql = []) { // {{{

		$config = include PATH . '/config/db.php';

		if(!isset($mysql['host'])) { $mysql['host'] = $config['host']; }
		if(!isset($mysql['username'])) { $mysql['username'] = $config['username']; }
		if(!isset($mysql['passwd'])) { $mysql['passwd'] = $config['passwd']; }
		if(!isset($mysql['dbname'])) { $mysql['dbname'] = $config['dbname']; }

		if($config['suppress_version_mismatch']) {
			// Supressing the version mismatch

			$this->connection = @new \mysqli($mysql['host'], $mysql['username'], $mysql['passwd'], $mysql['dbname']);

		} else {

			$this->connection = new \mysqli($mysql['host'], $mysql['username'], $mysql['passwd'], $mysql['dbname']);

		}

		$this->statements = new \Freemwurk\Core\dataClass();

	} // }}}

	/**
	 * Returns the MySQLi connection object.
	 * @return mysqli
	 */
	public function mysqli() { // {{{

		return $this->connection;

	} // }}}

	public function stmt($name = 'stmt') { // {{{

		return $this->statements->$name;

	} // }}}

	/**
	 * Escape a given string.
	 * @param string $string
	 * @return string
	 */
	public function escape($string) { // {{{

		return mysqli_real_escape_string($string);

	} // }}}

	/**
	 * Run a query in the database.
	 * @param string $query The query you want to execute.
	 * @return mysqli_result The result of the executed query.
	 */
	public function query($query) { // {{{

		return $this->connection->query($query);

	} // }}}

	public function prep($query, $name = 'stmt') { // {{{

		$this->statements->$name = $this->connection->prepare($query);

		if($this->statements->$name) {

			return true;

		} else {

			return false;

		}

	} // }}}

}

