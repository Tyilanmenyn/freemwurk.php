<?php
/**
 * @author Patrick "Tyil" Spek <p.spek@tyil.nl>
 */

namespace Freemwurk\Objects;

class EmailObject {

	private $to
	      , $cc
	      , $bcc
	      , $from
	      , $subject
	      , $body
	      , $headers;

	public function __construct() { // {{{

		$config = include PATH . '/config/email.php';

		$this->to = $this->cc = $this->bcc = [];
		$this->subject = 'Nondescript';
		$this->body = '';
		$this->headers = 'MIME-Version: 1.0' . "\r\n";

		if(isset($config['from'])) {

			$this->from = $config['from'];

		}

		if($config['html']) {

			$this->header('Content-type: text/html; charset=iso-8859-1');

		}

	} // }}}

	public function from($email, $name = false) { // {{{

		if($name) {

			$this->from = $name . ' <' . $email . '>';

		} else {

			$this->from = $email;

		}

	} // }}}

	public function to($email, $name = false) { // {{{

		if($name) {

			$this->to[] = $name . ' <' . $email . '>';

		} else {

			$this->to[] = $email;

		}

	} // }}}

	public function cc($email, $name = false) { // {{{

		if($name) {

			$this->cc[] = $name . ' <' . $email . '>';

		} else {

			$this->cc[] = $email;

		}

	} // }}}

	public function bcc($email, $name = false) { // {{{

		if($name) {

			$this->bcc[] = $name . ' <' . $email . '>';

		} else {

			$this->bcc[] = $email;

		}

	} // }}}

	public function subject($subject) { // {{{

		$this->subject = $subject;

	} // }}}

	public function body($message) { // {{{

		$this->body .= $message;

	} // }}}

	public function header($header) { // {{{

		$this->headers .= $header . "\r\n";

	} // }}}

	public function send() { // {{{

		if(isset($this->from)) {

			$this->header('From: ' . $this->from);

		}

		if(count($this->to) > 1) {

			$to = implode(', ', $this->to);

		} else {

			$to = $this->to[0];

		}

		if(count($this->cc) > 0) {

			if(count($this->cc) > 1) {

				$cc = implode(', ', $this->cc);

			} else {

				$cc = $this->cc[0];

			}

			$this->header('Cc: ' . $cc);

		}

		if(count($this->bcc) > 0) {

			if(count($this->bcc) > 1) {

				$bcc = implode(', ', $this->bcc);

			} else {

				$bcc = $this->bcc[0];

			}

			$this->header('Bcc: ' . $bcc);

		}

		return mail($to, $this->subject, $this->body, $this->headers);

	} // }}}

}
