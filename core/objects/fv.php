<?php

namespace Freemwurk\Objects;

class FvObject {

	private $rules;
	private $valid;
	private $errors;

	public function __construct() { // {{{

		$this->rules  = [];
		$this->valid  = true;
		$this->errors = [];

	} // }}}

	private function setError($message) { // {{{

		$this->valid    = false;
		$this->errors[] = $message;

	} // }}}

	private function required($field, $value, $error) { // {{{

		if(!empty($_POST[$field]) || !empty($_FILES[$field])) {

			if($_POST[$field] != '' || $_FILES[$field] != '') {

				return;

			}

		}

		$this->setError($error);
		return;

	} // }}}

	private function minlen($field, $value, $error) { // {{{

		if(!empty($_POST[$field])) {

			if(strlen($_POST[$field]) > $value) {

				return;

			}

		}

		$this->setError($error);

	} // }}}

	private function maxlen($field, $value, $error) { // {{{

		if(!empty($_POST[$field])) {
			
			if($value > strlen($_POST[$field])) {

				return;

			}

		}

		$this->setError($error);

	} // }}}

	private function match($field, $value, $error) { // {{{

		if(!empty($_POST[$field])) {

			if(substr($value, 0, 6) == 'field:') {

				if($_POST[$field] == substr($value, 6)) {

					return;

				}

			} else {

				if($_POST[$field] == $value) {

					return;

				}

			}

		}

		$this->setError($error);

	} // }}}

	private function email($field, $value, $error) { // {{{

		if(!empty($_POST[$field])) {

			// Using the build-in email validation
			if(filter_var($_POST[$field], FILTER_VALIDATE_EMAIL)) {

				return;

			}

		}

		$this->setError($error);

	} // }}}

	/**
	 * Add a rule to validate
	 *
	 * @param string $field
	 * @param string $rule
	 * @param string $error
	 * @access public
	 * @return void
	 */
	public function rule($field, $rule, $error = '') { // {{{

		$value = null;

		if(strpos($rule, ':') !== false) {

			$e     = explode(':', $rule, 1);
			$rule  = $e[0];
			$value = $e[1];

		}

		$this->rules[] = [
			'field' => $field,
			'rule'  => $rule,
			'value' => $value,
			'error' => $error
		];

		return $this;

	} // }}}

	/**
	 * Retrieve an array with error messages
	 *
	 * @access public
	 * @return void
	 */
	public function getErrors() { // {{{

		return $this->errors;

	} // }}}

	public function validate() { // {{{

		if(!empty($_POST) || !empty($_FILES)) {

			if(count($this->rules) > 0) {

				foreach($this->rules as $ruleset) {

					if(method_exists($this, $ruleset['rule'])) {

						$this->$ruleset['rule']($ruleset['field'], $ruleset['value'], $ruleset['error']);

					}

				}

			}

			return $this->valid;

		}

		return false;

	} // }}}

}

