<?php
/**
 * @author Patrick "Tyil" Spek <p.spek@tyil.nl>
 */

namespace Freemwurk\Objects;

class FormvalidatorObject {

	private $rules;
	private $error;

	public function __construct() { // {{{

		$this->rules = new \Freemwurk\Core\dataClass();
		$this->error = new \Freemwurk\Core\dataClass();

	} // }}}

	private function required($field) { // {{{

		if((isset($_POST[$field]) && $_POST[$field] != '') || (isset($_FILES[$field]))) {

			return true;

		}

		return false;

	} // }}}

	private function range($field, $param) { // {{{

		$range = explode('-', $param);

		if($_POST[$field] > $range[0] && $_POST[$field] < $range[1]) {

			return true;

		}

		return false;

	} // }}}

	private function minval($field, $param) { // {{{

		if($_POST[$field] > $param) {

			return true;

		}

		return false;

	} // }}}

	private function maxval($field, $param) { // {{{

		if($_POST[$field] < $param) {

			return true;

		}

		return false;

	} // }}}

	private function minlen($field, $param) { // {{{

		if(strlen($_POST[$field]) > $param) {

			return true;

		}

		return false;

	} // }}}

	private function maxlen($field, $param) { // {{{

		if(strlen($_POST[$field]) < $param) {

			return true;

		}

		return false;

	} // }}}

	private function match($field, $param) { // {{{

		if(strpos('field:', $param) !== false) {

			# We should check if we could just use a regex like this:
			#  /post:/(.*)

		} else {

			return ($_POST[$field] == $param);

		}

	} // }}}

	private function email($field) { // {{{

		return (preg_match('/.*\@.*/', $_POST[$field]) != 0);

	} // }}}

	/**
	 * Attempt to validate the form.
	 * @return boolean If everything validated fine, TRUE will be returned. If
	 *         there was no form data, it will simply return FALSE. If an error
	 *         occurred while validating, FALSE will be returned, and the field
	 *         and failure will be set in $_SESSION->tmp->validation_error.
	 */
	public function validate() { // {{{

		if(@$_POST) {

			$ruleArray = $this->rules->toArray();

			foreach($ruleArray as $field => $ruleset) {

				$rules = explode(';', $ruleset);

				foreach($rules as $rule) {

					if(strpos($rule, ',') !== false) {

						$arr = explode(',', $rule);

						if(method_exists($this, $arr[0])) {

							if(!$this->$arr[0]($field, $arr[1])) {

								\Freemwurk\Core\Session::set('validation/error', true, 'tmp');
								\Freemwurk\Core\Session::set('validation/field', $field,               'tmp');
								\Freemwurk\Core\Session::set('validation/func',  $arr[0],              'tmp');
								\Freemwurk\Core\Session::set('validation/param', $arr[1],              'tmp');
								\Freemwurk\Core\Session::set('validation/msg',   $this->error->$field, 'tmp');

								return false;

							}

						}

					} else {

						if(method_exists($this, $rule)) {

							if(!$this->$rule($field)) {

								\Freemwurk\Core\Session::set('validation/error', true, 'tmp');
								\Freemwurk\Core\Session::set('validation/field', $field,               'tmp');
								\Freemwurk\Core\Session::set('validation/func',  $rule,              'tmp');
								\Freemwurk\Core\Session::set('validation/msg',   $this->error->$field, 'tmp');

								return false;

							}

						}

					}

				}

			}

			return true;

		} else {

			return false;

		}

	} // }}}

	/**
	 * Add a rule to be validated.
	 * @param string $field The field which should be validated.
	 * @param string $rules The rules, seperated by a ";".
	 */
	public function rule($field, $rules, $error = false) { // {{{

		$this->rules->$field = $rules;

		if(!$error) {

			$this->error->$field = 'An undefined error ocurred at the field "' . $field . '".';

		} else {

			$this->error->$field = $error;

		}

	} // }}}

}
