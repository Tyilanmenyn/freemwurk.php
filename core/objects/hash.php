<?php 
/**
 * @author Patrick "Tyil" Spek <p.spek@tyil.nl>
 */

namespace Freemwurk\Objects;

class HashObject {

	public function salt($size, $extendedPool = '') { // {{{

		$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' . $extendedPool;
		$poolSize = strlen($pool) - 1;
		$salt = '';

		for($i = 0; $i < $size; $i++) {

			$salt .= $pool[mt_rand(0, $poolSize)];

		}

		return $salt;

	} // }}}

	public function verify($string, $hash) { // {{{

		return ($hash == crypt($string, $hash));

	} // }}}

	public function bcrypt($string, $salt = '', $rounds = 12) { // {{{

		if(defined('CRYPT_BLOWFISH') && CRYPT_BLOWFISH == 1) {

			if($salt == '') { $salt = $this->salt(22); }
			if($rounds < 9) { $rounds = '0' . (string)$rounds; }

			return crypt($string, '$2y$' . $rounds . '$' . $salt . '$');

		} else {

			trigger_error('<strong>bcrypt</strong> is not supported.', E_USER_ERROR);

			return false;

		}

	} // }}}

	public function md5($string) { // {{{

		return md5($string);

	} // }}}

	public function sha1($string) { // {{{

		return sha1($string);

	} // }}}

}
