<?php

namespace Freemwurk\Objects;

class PdoObject {

	private $dbh;
	private $sth;
	private $qq;

	public function __construct() { // {{{

		$this->sth = [];
		$this->qq  = new \stdClass();

	} // }}}

	private function dsn($driver, $database, $host, $port, $charset='utf8') { // {{{

		$dsn  = $driver;
		$dsn .= ':dbname=' . $database;
		$dsn .= ';host=' . $host;
		$dsn .= ';port=' . $port;
		$dsn .= ';charset=' . $charset;

		return $dsn;

	} // }}}

	private function createPDO($connection = 'main') { // {{{

		$config = include PATH . '/config/pdo.php';

		if(isset($config[$connection])) {

			try {

				return new \PDO(
					$this->dsn(
						$config[$connection]['driver'],
						$config[$connection]['database'],
						$config[$connection]['host'],
						$config[$connection]['port']
					),
					$config[$connection]['username'],
					$config[$connection]['password']
				);

			} catch(\PDOException $e) {

				throw new \Exception($e->getMessage());

			}

		} else {

			throw new \Exception('No config set for connection "' . $connection . '"');

		}

	} // }}}

	private function qqClear() { // {{{

		$this->qq = null;

	} // }}}

	private function qqGenerate() { // {{{

		// Define the possible qqTypes
		$qqTypes = ['create', 'read', 'update', 'delete'];

		if(in_array($this->qq->type, $qqTypes)) {

			switch($this->qq->type) {

				case 'create': // {{{
					$query  = $this->qqParseInsert();
					break; // }}}
				case 'read': // {{{
					$query  = $this->qqParseSelect();
					$query .= $this->qqParseFrom();
					$query .= $this->qqParseWhere();
					$query .= $this->qqParseOrderBy();
					$query .= $this->qqParseLimit();
					break; // }}}
				case 'update': // {{{
					$query  = $this->qqParseUpdate();
					$query .= $this->qqParseSet();
					$query .= $this->qqParseWhere();
					break; // }}}
				case 'delete': // {{{
					$query  = $this->qqParseDelete();
					$query .= $this->qqParseFrom();
					$query .= $this->qqParseWhere();
					break; // }}}

			}

			return $query . ';';

		} else {

			throw new \Exception('Invalid quick-query type');
			return;

		}

	} // }}}

	private function qqParseDelete() { // {{{

		return 'DELETE ';

	} // }}}

	private function qqParseFrom() { // {{{

		if(isset($this->qq->from)) {

			$query = ' FROM `' . $this->qq->from . '`';

			return $query;

		}

		return '';

	} // }}}

	private function qqParseInsert() { // {{{

		$fields = [];

		foreach($this->qq->insertInto['values'] as $field => $value) {

			$fields[] = '`' . $field . '`';
			$values[] = ':' . $field;

		}

		$query  = 'INSERT INTO ';
		$query .= '`' . $this->qq->insertInto['table'] . '` ';
		$query .= '(' . implode(', ', $fields) . ') VALUES ';
		$query .= '(' . implode(', ', $values) . ')';

		return $query;

	} // }}}

	private function qqParseLimit() { // {{{

		if(isset($this->qq->limit)) {

			return ' LIMIT ' . $this->qq->limit['start'] . ', ' . $this->qq->limit['rows'];

		}

		return '';

	} // }}}

	private function qqParseOrderBy() { // {{{

		if(isset($this->qq->orderBy)) {

			$query = ' ORDER BY `' . $this->qq->orderBy[0] . '` ' . strtoupper($this->qq->orderBy[1]);

			return $query;

		}

		return '';

	} // }}}

	private function qqParseSelect() { // {{{

		$selects = [];
		foreach($this->qq->select as $k => $v) {

			if(is_string($k)) {

				$selects[] = '`' . $k . '` AS `' . $v . '`';

			} else {

				$selects[] = '`' . $v . '`';

			}

		}

		if(count($selects) > 0) {

			$query = implode($selects, ', ');

		} else {

			$query = $selects[0];

		}

		return 'SELECT ' . $query;

	} // }}}

	private function qqParseSet() { // {{{

		if(isset($this->qq->set) && count($this->qq->set) > 0) {

			$updates = [];

			foreach($this->qq->set as $field => $value) {

				$updates[] = '`' . $field . '`=:' . $field;

			}

			if(count($updates) > 1) {

				$updates = implode(', ', $updates);

			} else {

				$updates = $updates[0];

			}

			return ' SET ' . $updates;

		}

		return '';

	} // }}}

	private function qqParseUpdate() { // {{{

		return 'UPDATE `' . $this->qq->update . '`';

	} // }}}

	private function qqParseWhere() { // {{{

		if(isset($this->qq->where)) {

			$query = ' WHERE';
			$where  = [];

			foreach($this->qq->where as $k => $v) {

				$where[] = '`' . $k . '`=:' . $k;

			}

			if(count($this->qq->where) > 1) {

				$query .= ' ' . implode(' AND ');

			} else {

				$query .= ' ' . $where[0];

			}

			return $query;

		}

		return '';

	} // }}}

	private function qqParseWhereParams(&$sth) { // {{{

		if(isset($this->qq->where)) {

			foreach($this->qq->where as $k => $v) {

				switch(gettype($v)) {

					         case 'boolean': $type = \PDO::PARAM_BOOL; break;
					         case 'integer': $type = \PDO::PARAM_INT;  break;
					         case 'null'   : $type = \PDO::PARAM_NULL; break;
					default: case 'string' : $type = \PDO::PARAM_STR;  break;

				}

				$sth->bindParam($k, $this->qq->where[$k], $type);

			}

		}

	} // }}}

	private function qqParseInsertParams(&$sth) { // {{{

		if(isset($this->qq->insertInto['values'])) {

			foreach($this->qq->insertInto['values'] as $k => $v) {

				switch(gettype($v)) {

					         case 'boolean': $type = \PDO::PARAM_BOOL; break;
					         case 'integer': $type = \PDO::PARAM_INT;  break;
					         case 'null'   : $type = \PDO::PARAM_NULL; break;
					default: case 'string' : $type = \PDO::PARAM_STR;  break;

				}

				$sth->bindParam(
					$k,
					$this->qq->insertInto['values'][$k],
					$type
				);

			}

		}

	} // }}}

	private function qqParseSetParams(&$sth) { // {{{

		if(isset($this->qq->set)) {

			foreach($this->qq->set as $k => $v) {

				switch(gettype($v)) {

					         case 'boolean': $type = \PDO::PARAM_BOOL; break;
					         case 'integer': $type = \PDO::PARAM_INT;  break;
					         case 'null'   : $type = \PDO::PARAM_NULL; break;
					default: case 'string' : $type = \PDO::PARAM_STR;  break;

				}

				$sth->bindParam($k, $this->qq->set[$k], $type);

			}

		}

	} // }}}

	/**
	 * Change the current connection to the one passed to this method
	 *
	 * @param string $connection
	 * @access public
	 * @return void
	 */
	public function conn($connection = 'main') { // {{{

		$this->dbh = $this->createPDO($connection);

	} // }}}

	/**
	 * Return a statement-handle.
	 *
	 * @param string $name The name of the handle you wish to retrieve.
	 * @access public
	 * @return void
	 */
	public function s($name = 'sth') { // {{{

		return $this->sth[$name];

	} // }}}

	/**
	 * Prepare a query
	 *
	 * @param string $query The query you wish to prepare.
	 * @param string $name The name of the statement-handle.
	 * @access public
	 * @return bool True on success, false on failure.
	 */
	public function p($query, $name = 'sth') { // {{{

		if(!isset($this->dbh)) {
			// Create a Database handle in case we didn't do that yet

			$this->dbh = $this->createPDO();

		}

		$this->sth[$name] = $this->dbh->prepare($query);

		if($this->sth[$name] !== false) {

			return true;

		}

		return false;

	} // }}}

	/**
	 * Execute a prepared statement
	 *
	 * @param mixed $values An array of values to be passed to the query.
	 * @param string $name The name of the statement-handle.
	 * @access public
	 * @return bool True on success, false on failure.
	 */
	public function x($values = [], $name = 'sth') { // {{{

		return $this->sth[$name]->execute($values);

	} // }}}

	/**
	 * Bind a parameter to a given statement-handle.
	 *
	 * @param mixed $parameter The prepared parameter to bind the variable to.
	 * @param mixed $variable The variable to be bound.
	 * @param mixed $type Any of the PDO::PARAM_* values. Defaults to string.
	 * @param string $name The name of the statement-handle.
	 * @access public
	 * @return void
	 */
	public function b($parameter, &$variable, $type = \PDO::PARAM_STR, $name = 'sth') { // {{{

		$this->sth[$name]->bindParam($parameter, $variable, $type);

	} // }}}

	/**
	 * Return the ID of the last inserted row.
	 *
	 * @param mixed $name This is only needed when using the PGSQL driver.
	 * @access public
	 * @return int
	 */
	public function id($name = null) { // {{{

		return $this->dbh->lastInsertId($name);

	} // }}}

	/**
	 * Start a transaction.
	 *
	 * @access public
	 * @return bool
	 */
	public function ts() { // {{{

		if(!isset($this->dbh)) {
			// Create a Database handle in case we didn't do that yet

			$this->dbh = $this->createPDO();

		}

		return $this->dbh->beginTransaction();

	} // }}}

	/**
	 * Commit the transaction
	 *
	 * @access public
	 * @return bool
	 */
	public function tc() { // {{{

		return $this->dbh->commit();

	} // }}}

	/**
	 * Rollback the transaction
	 *
	 * @access public
	 * @return bool
	 */
	public function tr() { // {{{

		try {

			return $this->dbh->rollBack();

		} catch(\PDOException $e) {

			trigger_error('An error has occurred in the PDO object: ' . $e->getMessage(), E_USER_NOTICE);
			return false;

		}

	} // }}}

	/**
	 * Fetch a row from a statement.
	 *
	 * @param mixed $type
	 * @param mixed $orientation
	 * @param mixed $offset
	 * @access public
	 * @return void
	 */
	public function f($name = 'sth', $type = \PDO::FETCH_OBJ, $orientation = \PDO::FETCH_ORI_NEXT, $offset = null) { // {{{

		return $this->sth[$name]->fetch($type, $orientation, $offset);

	} // }}}

	/**
	 * Fetch all remaining rows from a statement.
	 *
	 * @param mixed $type
	 * @param string $name
	 * @access public
	 * @return void
	 */
	public function fa($name = 'sth', $type = \PDO::FETCH_OBJ) { // {{{

		return $this->sth[$name]->fetchAll($type);

	} // }}}

	/**
	 * Return the rowcount of a statement.
	 *
	 * @param string $name
	 * @access public
	 * @return int
	 */
	public function rc($name = 'sth') { // {{{

		return $this->sth[$name]->rowCount();

	} // }}}
	
	/**
	 * Close a given statement.
	 *
	 * @param string $name
	 * @access public
	 * @return void
	 */
	public function c($name = 'sth') { // {{{

		$this->sth[$name]->closeCursor();

	} // }}}

	/**
	 * Specify a database config to use in a quick-query
	 *
	 * @param string $conn
	 * @access public
	 * @return void
	 */
	public function using($conn = 'main') { // {{{

		$this->qq->connection = $conn;

		return $this;

	} // }}}

	/**
	 * Specify an array of columns to select. You can use a hashed array to Specify
	 * an "as" statement.
	 *
	 * @param array $select An array of columns to select
	 * @access public
	 * @return void
	 */
	public function select($select) { // {{{

		if(is_array($select)) {

			$this->qq->type = 'read';
			$this->qq->select = $select;

		} else {

			throw new \Exception('select() requires 1 array argument');

		}

		return $this;

	} // }}}

	/**
	 * Specify the table to update
	 *
	 * @param string $table
	 * @access public
	 * @return void
	 */
	public function update($table) { // {{{

		$this->qq->type = 'update';
		$this->qq->update = $table;

		return $this;

	} // }}}

	/**
	 * Set the update values in the form of ['column' => 'newVal']
	 *
	 * @param mixed $set
	 * @access public
	 * @return void
	 */
	public function set($set) { // {{{

		if(is_array($set)) {

			$this->qq->set = $set;

		} else {

			throw new \Exception('set() requires 1 array argument');

		}

		return $this;

	} // }}}

	/**
	 * Set the limit of the query
	 * 
	 * @param int $start
	 * @param int $rows
	 * @access public
	 * @return void
	 */
	public function limit($start = 0, $rows = 30) { // {{{

		if($start >= 0 && $rows > 0) {

			$this->qq->limit['start'] = $start;
			$this->qq->limit['rows']  = $rows;

		} else {

			throw new \Exception('Invalid limit() settings');

		}

		return $this;

	} // }}}

	/**
	 * Initiate a delete-query
	 *
	 * @access public
	 * @return void
	 */
	public function delete() { // {{{

		$this->qq->type = 'delete';

		return $this;

	} // }}}

	/**
	 * Specify from which table to select
	 *
	 * @param string $table
	 * @access public
	 * @return void
	 */
	public function from($table) { // {{{

		$this->qq->from = $table;

		return $this;

	} // }}}

	/**
	 * An array of filters for the table contents
	 *
	 * @param array $where
	 * @access public
	 * @return void
	 */
	public function where($where) { // {{{

		if(is_array($where)) {

			$this->qq->where = $where;

		} else {

			throw new \Exception('where() requires 1 array argument');

		}

		return $this;

	} // }}}

	/**
	 * Specify on which column to sort, and in what direction
	 *
	 * @param string $field
	 * @param string $order
	 * @access public
	 * @return void
	 */
	public function orderBy($field, $order = 'asc') { // {{{

		if($order == 'asc' || $order == 'desc') {

			$this->qq->orderBy = [$field, $order];

		} else {

			throw new \Exception('orderBy() argument 2 must be \'asc\' or \'desc\'');

		}

		return $this;

	} // }}}

	/**
	 * Insert associative array $values into $table
	 *
	 * @param string $table
	 * @param mixed $values
	 * @access public
	 * @return void
	 */
	public function insertInto($table, $values) { // {{{

		@$this->qq->type = 'create';

		$this->qq->insertInto = [
			'table'  => $table,
			'values' => $values
		];

		return $this;

	} // }}}

	/**
	 * Run a quick-query
	 *
	 * @access public
	 * @return void
	 */
	public function run() { // {{{

		if(!isset($this->qq->connection)) {

			$this->using('main');

		}

		try {

			$dbh   = $this->createPDO($this->qq->connection);
			$query = $this->qqGenerate();

			if($sth = $dbh->prepare($query)) {

				// Parse parameters where needed
				$this->qqParseSetParams($sth);
				$this->qqParseWhereParams($sth);
				$this->qqParseInsertParams($sth);

				$dbh->beginTransaction();

				if($sth->execute()) {

					switch($this->qq->type) {
						case 'create': $return = $dbh->lastInsertId(); break;
						case 'read':   $return = $sth->fetchAll(\PDO::FETCH_CLASS); break;
						case 'update': $return = true; break;
						case 'delete': $return = true; break;
					}

					// Clear all the used variables
					$dbh->commit();
					$this->qqClear();

					return $return;

				} else {

					$dbh->rollBack();

					if(isset($sth->errorCode)) {

						throw new \Exception('Error during execute: ' . $sth->errorCode . ': ' . $sth->errorInfo);

					}

				}

				return null;

			} else {

				throw new Exception('Query could not be prepared: ' . $query);

			}

		} catch(\PDOException $e) {

			throw new Exception($e->getMessage());

		}

	} // }}}

}

