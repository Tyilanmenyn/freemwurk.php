<?php
/**
 * @package Freemwurk
 * @since v1.0
 * @author Patrick "Tyil" Spek <p.spek@tyil.nl>
 * This is the main controller class. All other controllers are extensions of
 * this class, and therefore inherit all functionality.
 */

namespace Freemwurk\Core;

class Controller extends Loader {

	public $loadDefault;

	public function __construct() { // {{{

		parent::__construct();

		$this->loadDefault = 'index';

		$this->autoload();

	} // }}}

}
