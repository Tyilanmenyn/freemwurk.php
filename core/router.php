<?php

namespace Freemwurk\Core;

$queryStringArray = explode('/', $_SERVER['QUERY_STRING']);

$config = include PATH . '/config/router.php';
$nsControllers = '\Freemwurk\Controllers\\';

if(count($queryStringArray) > 0 && $queryStringArray[0] != '') {

        $i = 1;
        $c = count($queryStringArray);
        $file = $queryStringArray[0];
        $cont = $nsControllers . ucfirst($queryStringArray[0]);
        $func = strtolower(@$queryStringArray[1]);

        while(!file_exists(PATH . '/content/controllers/' . $file . '.php')) {

                if($i > $c) {
                        // File is not found

                        $file = $config['error_controller'];
                        $arr = explode('/', $config['error_controller']);
                        $cont = $nsControllers . ucfirst($arr[count($arr) - 1]);
                        $func = $config['error_method'];

                        break;

                } else {

                        $file .= '/' . @$queryStringArray[$i];
                        $cont = $nsControllers . ucfirst(@$queryStringArray[$i++]);
                        $func = strtolower(@$queryStringArray[($i)]);

                }

        }

        while($i++ < $c) { $contVars[] = @$queryStringArray[$i]; }

} else {
        // Load the default controller

        $file = $config['file'];
        $cont = $nsControllers . $config['cont'];
        $func = $config['func'];

}

require PATH . '/content/controllers/' . $file . '.php';

$cont .= 'Controller';

$controller = new $cont();

if(@$func == '') {

        $func = $controller->loadDefault;

} else {

        $func .= 'Method';

}

if(method_exists($controller, $func)) {

        if(!@$contVars) { $contVars = []; }

        call_user_func_array([$controller, $func], $contVars);

} else {

        if(method_exists($controller, $controller->loadDefault)) {

                // Remove the "Method" part, and prepend the variable to our array
                array_unshift($contVars, substr($func, 0, -6));

                // Load the default controller with all given arguments
                call_user_func_array([$controller, $controller->loadDefault], $contVars);

        } else {

                $file = $config['error_controller'];
                $arr = explode('/', $config['error_controller']);
                $cont = $nsControllers . ucfirst($arr[count($arr) - 1]);
                $func = $config['error_method'];

                require_once PATH . '/content/controllers/' . $file . '.php';

                $cont .= 'Controller';

                $controller = new $cont();

                $controller->$func();

        }

}


