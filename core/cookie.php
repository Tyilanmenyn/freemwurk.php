<?php

namespace Freemwurk\Core;

class Cookie {

	private static $namespace;

	public static function init($namespace = 'userdata') { // {{{

		self::$namespace = $namespace;

	} // }}}

	public static function setNamespace($namespace) { // {{{

		self::$namespace = $namespace;

	} // }}}

	public static function getNamespace() { // {{{

		return self::$namespace;

	} // }}}

	public static function set($key, $val, $namespace = '') { // {{{

		$ns = $namespace != '' ? $namespace : self::$namespace;

		$_COOKIE[$ns][$key] = $val;

	} // }}}

	public static function get($key, $namespace = '') { // {{{

		$ns = $namespace != '' ? $namespace : self::$namespace;

		return $_COOKIE[$ns][$key];

	} // }}}

	public static function clear($namespace = '') { // {{{

		if($namespace != '') {

			$_COOKIE[$namespace] = null;

		} else {

			$_COOKIE = null;

		}

	} //}}}

}

