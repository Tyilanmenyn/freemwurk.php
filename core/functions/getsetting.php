<?php

namespace Freemwurk\Functions;

function getSetting($name) {

	// Get user-level defined value
	if(defined(FW_SESSIONMANAGER)) { // {{{

		if(\Freemwurk\Core\Session::get('user_id') > 0) {

			if(\Freemwurk\Core\Session::get($name) != '') {

				return \Freemwurk\Core\Session::get($name);

			}

		}

	} // }}}

	// Get cookie-level defined value
	if(defined(FW_COOKIEMANAGER)) { // {{{

		if(\Freemwurk\Core\Cookie::get($name) != '') {

			return \Freemwurk\Core\Cookie::get($name);

		}

	} // }}}

	// Get global-level defined value
	if(defined($name)) { // {{{

		return constant($name);

	} elseif(defined(strtoupper($name))) {
	
		return constant(strtoupper($name));

	} // }}}

	return '';

}

