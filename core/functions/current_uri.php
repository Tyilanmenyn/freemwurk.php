<?php

namespace Freemwurk\Functions;

/**
 * Return the current URI.
 * @global string $file
 * @global string $func
 * @param bool $withVars Set this to true if you want all variables to be
 *                       included as well.
 * @return string
 */
function current_uri($withVars = false) {

	if($withVars) {

		$return = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://');
		$return .= $_SERVER['SERVER_NAME'];
		if($_SERVER['SERVER_PORT'] != 80) { $return .= $_SERVER['SERVER_PORT']; }
		$return .= $_SERVER['REQUEST_URI'];

	} else {

		global $file, $func; // These are taken from the router

		$return = URL . '/' . $file . '/' . substr($func, 0, -6);

	}

	return $return;

}

