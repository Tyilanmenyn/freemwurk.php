<?php

namespace Freemwurk\Functions;

/**
 * Redirect to the target $url using a location-header.
 * @param string $url
 */
function redirect($url) {

	('Location: ' . $url);

}
