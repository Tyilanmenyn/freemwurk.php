<?php

namespace Freemwurk\Functions\Form;

/**
 * Create an opening form tag.
 * @param string $action The location to where the formdata should be send.
 * @param bool $multipart If set to TRUE, the form tag will include the
 *                        multipart attribute, allowing files to be send as well.
 * @param string $class If set, the form will include a class attribute.
 * @param string $id If set, the form will include an id attribute.
 * @param string $method Defaults to POST, but can also be set to GET.
 * @param string $charset Defaults to utf-8, but can be set to any charset you
 *                        wish.
 * @return string The completed form tag.
 */
function open($action = '', $multipart = false, $class = '', $id = '', $method = 'post', $charset = 'utf-8') {

	if($action == '') {
		// If no action is given, make it the current URI

		$action = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://');
		$action .= $_SERVER['SERVER_NAME'];
		if($_SERVER['SERVER_PORT'] != 80) { $action .= $_SERVER['SERVER_PORT']; }
		$action .= $_SERVER['REQUEST_URI'];

	}

	$return = '<form action="' . $action . '" method="' . $method . '" accepted-charset="' . $charset . '"';

	if($multipart) {

		$return .= ' enctype="multipart/form-data"';

	}

	if($class != '') {

		$return .= ' class="' . $class . '"';

	}

	if($id != '') {

		$return .= ' id="' . $id . '"';

	}

	$return .= '>';

	return $return;

}
