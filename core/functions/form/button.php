<?php

namespace Freemwurk\Functions\Form;

/**
 * Create a simple button tag.
 * @param string $value The value of the button.
 * @param string $id An optional id for the button.
 * @param string $onClick An optional JavaScript call bound to the onClick event.
 * @param array $additionalAttributes An optional array of additional attributes.
 * @return string The completed button tag.
 */
function button($value, $id = '', $onClick = '', $additionalAttributes = []) {

	$return = '<button';

	if($id != '') {

		$return .= ' id="' . $id . '"';

	}

	if(count($additionalAttributes) > 0) {

		foreach($additionalAttributes as $attribute => $value) {

			$return .= ' ' . $attribute . '="' . $value . '"';

		}

	}

	if($onClick != '') {

		$return .= ' onclick="' . $onClick . '"';

	}

	$return .= '>' . $value . '</button>';

	return $return;

}
