<?php

namespace Freemwurk\Functions\Form;

/**
 * form_select 
 * 
 * @param string $name The name of the select field.
 * @param array $options An array in the style of $value => $name.
 * @param string $default Not required. Should be a valid $value to use for a
 *                        default.
 * @access public
 * @return string
 */
function select($name, $options, $default = '', $additionalAttributes = []) {

	$return = '<select name="' . $name . '"';

	if(count($additionalAttributes) > 0) {

		foreach($additionalAttributes as $attribute => $value) {

			$return .= ' ' . $attribute . '="' . $value . '"';

		}

	}

	$return .= '>';

	foreach($options as $value => $name) {

		$return .= '<option value="' . $value . '"';

		if($default == $value) {

			$return .= ' selected="selected"';

		}

		$return .= '>' . $name . '</option>';

	}

	$return .= '</select>';

	return $return;

}

