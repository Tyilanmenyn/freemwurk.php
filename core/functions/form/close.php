<?php

namespace Freemwurk\Functions\Form;

/**
 * Simply returns a closing form tag.
 * @return string "</form>"
 */
function close() {

	return '</form>';

}
