<?php

namespace Freemwurk\Functions\Form;

/**
 * Create a textarea tag.
 * @param string $name The name of the textarea
 * @param string $placeholder An optional placeholder.
 * @param string $value An optional value.
 * @param bool $required Set to true if the field should be required.
 * @param array $additionalAttributes An optional array of additional attributes.
 * @return string The completed form tag.
 */
function textarea($name, $placeholder = '', $default = '', $required = false, $additionalAttributes = []) {

	$return = '<textarea name="' . $name . '"';

	if($placeholder != '') {

		$return .= ' placeholder="' . $placeholder . '"';

	}

	if(count($additionalAttributes) > 0) {

		foreach($additionalAttributes as $attribute => $value) {

			$return .= ' ' . $attribute . '="' . $value . '"';

		}

	}

	if($required) {

		$return .= ' required';

	}

	$return .= '>' . $default . '</textarea>';

	return $return;

}
