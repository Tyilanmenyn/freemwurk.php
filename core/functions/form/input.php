<?php

namespace Freemwurk\Functions\Form;

/**
 * Create a basic form input field.
 * @param string $type The type of the input field.
 * @param string $name The name of the input field.
 * @param string $placeholder An optional placeholder.
 * @param string $value An optional value.
 * @param bool $required Set to true if the field should be required.
 * @param string $size Sets the width of the field in lenght of characters.
 * @param string $maxlength Sets the max length of characters allowed.
 * @param array $additionalAttributes An optional array of additional attributes.
 * @return string The completed input tag.
 */
function input($type, $name, $placeholder = '', $value = '', $required = false,
	$size = '25', $maxlength = null,  $additionalAttributes = []) {

	if(isset($maxlength)){

		$return = '<input type="' . $type . '" size="'.$size.'" maxlength="'.$maxlength.'" name="' . $name . '"';

	}else{

		$return = '<input type="' . $type . '" size="'.$size.'" name="' . $name . '"';

	}

	if($placeholder != '') {

		$return .= ' placeholder="' . $placeholder . '"';

	}

	if($value != '') {

		$return .= ' value="' . $value . '"';

	}

	if(count($additionalAttributes) > 0) {

		foreach($additionalAttributes as $attribute => $value) {

			$return .= ' ' . $attribute . '="' . $value . '"';

		}

	}

	if($required) {

		$return .= ' required';

	}

	$return .= ' />';

	return $return;

}

