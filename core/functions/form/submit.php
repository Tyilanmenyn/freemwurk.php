<?php

namespace Freemwurk\Functions\Form;

/**
 * Create a submit button.
 * @param string $value The value which the button will display.
 * @param string $name An optional name for the submit button.
 * @param array $additionalAttributes An optional array of additional attributes.
 * @return string The completed submit button.
 */
function submit($value, $name = '', $additionalAttributes = []) {

	$return = '<input type="submit" value="' . $value . '"';

	if($name != '') {

		$return .= ' name="' . $name . '"';

	}

	if(count($additionalAttributes) > 0) {

		foreach($additionalAttributes as $attribute => $value) {

			$return .= ' ' . $attribute . '="' . $value . '"';

		}

	}

	$return .= ' />';

	return $return;

}
