<?php

namespace Freemwurk\Functions;

function theme($theme = null) {

	if(isset($theme)) {

		if(FW_SESSIONMANAGER) {

			Session::set('theme', $theme);

		}

		if(FW_COOKIEMANAGER) {

			Cookie::set('theme', $theme);

		}

	} else {

		if(FW_SESSIONMANAGER && @Session::get('theme') != '') {

			return Session::get('theme');

		} elseif(FW_COOKIEMANAGER && @Cookie::get('theme') != '') {

			return Cookie::get('theme');

		} else {

			return THEME;

		}

	}

}

