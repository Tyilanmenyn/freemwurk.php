<?php

namespace Freemwurk\Core;

class Session {

	private static $namespace;

	public static function init($namespace = 'userdata') { // {{{

		session_start();

		self::$namespace = $namespace;

	} // }}}

	public static function destroy() { // {{{

		session_destroy();

	} // }}}

	public static function setNamespace($namespace) { // {{{

		self::$namespace = $namespace;

	} // }}}

	public static function getNamespace() { // {{{

		return self::$namespace;

	} // }}}

	public static function set($key, $val, $namespace = '') { // {{{

		$ns = $namespace != '' ? $namespace : self::$namespace;

		$_SESSION[$ns][$key] = $val;

	} // }}}

	public static function get($key, $namespace = '') { // {{{

		$ns = $namespace != '' ? $namespace : self::$namespace;

		return @$_SESSION[$ns][$key];

	} // }}}

	public function getAll($namespace) { // {{{

		return @$_SESSION[$namespace];

	} // }}}

	public static function clear($namespace = '') { // {{{

		if($namespace != '') {

			$_SESSION[$namespace] = null;

		} else {

			$_SESSION = null;

		}

	} // }}}

}

